﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t159784161;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1394852971_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1__ctor_m1394852971(__this, method) ((  void (*) (List_1_t2522024052 *, const MethodInfo*))List_1__ctor_m1394852971_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1198742556_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1198742556(__this, ___collection0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1198742556_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2126972244_gshared (List_1_t2522024052 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2126972244(__this, ___capacity0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1__ctor_m2126972244_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C"  void List_1__cctor_m1113350026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1113350026(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1113350026_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1918002125_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1918002125(__this, method) ((  Il2CppObject* (*) (List_1_t2522024052 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1918002125_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1952216609_gshared (List_1_t2522024052 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1952216609(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2522024052 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1952216609_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m868394352_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m868394352(__this, method) ((  Il2CppObject * (*) (List_1_t2522024052 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m868394352_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3146823117_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3146823117(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024052 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3146823117_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m474280595_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m474280595(__this, ___item0, method) ((  bool (*) (List_1_t2522024052 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m474280595_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m615988645_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m615988645(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024052 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m615988645_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m411008408_gshared (List_1_t2522024052 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m411008408(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2522024052 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m411008408_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1912599696_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1912599696(__this, ___item0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1912599696_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3879536340_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3879536340(__this, method) ((  bool (*) (List_1_t2522024052 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3879536340_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m857080475_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m857080475(__this, method) ((  Il2CppObject * (*) (List_1_t2522024052 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m857080475_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m86227298_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m86227298(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m86227298_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m456041327_gshared (List_1_t2522024052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m456041327(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2522024052 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m456041327_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C"  void List_1_Add_m1089213787_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1089213787(__this, ___item0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_Add_m1089213787_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1518088151_gshared (List_1_t2522024052 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1518088151(__this, ___newCount0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1518088151_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3810486997_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3810486997(__this, ___collection0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3810486997_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3071459221_gshared (List_1_t2522024052 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3071459221(__this, ___enumerable0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3071459221_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3137340898_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3137340898(__this, ___collection0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3137340898_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m3095953558_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_Clear_m3095953558(__this, method) ((  void (*) (List_1_t2522024052 *, const MethodInfo*))List_1_Clear_m3095953558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C"  bool List_1_Contains_m3175073495_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3175073495(__this, ___item0, method) ((  bool (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_Contains_m3175073495_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1849335948_gshared (List_1_t2522024052 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1849335948(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2522024052 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))List_1_CopyTo_m1849335948_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2541696822  List_1_GetEnumerator_m1383295158_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1383295158(__this, method) ((  Enumerator_t2541696822  (*) (List_1_t2522024052 *, const MethodInfo*))List_1_GetEnumerator_m1383295158_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3246901839_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3246901839(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_IndexOf_m3246901839_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3680447203_gshared (List_1_t2522024052 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3680447203(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3680447203_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1595703132_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1595703132(__this, ___index0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1595703132_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2397006339_gshared (List_1_t2522024052 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2397006339(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2397006339_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3092536376_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3092536376(__this, ___collection0, method) ((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3092536376_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C"  bool List_1_Remove_m4061378620_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m4061378620(__this, ___item0, method) ((  bool (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_Remove_m4061378620_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m270859209_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m270859209(__this, ___index0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_RemoveAt_m270859209_gshared)(__this, ___index0, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3230847821* List_1_ToArray_m1209652252_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_ToArray_m1209652252(__this, method) ((  Int32U5BU5D_t3230847821* (*) (List_1_t2522024052 *, const MethodInfo*))List_1_ToArray_m1209652252_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2719438728_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2719438728(__this, method) ((  int32_t (*) (List_1_t2522024052 *, const MethodInfo*))List_1_get_Capacity_m2719438728_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3023244265_gshared (List_1_t2522024052 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3023244265(__this, ___value0, method) ((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3023244265_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m2520315171_gshared (List_1_t2522024052 * __this, const MethodInfo* method);
#define List_1_get_Count_m2520315171(__this, method) ((  int32_t (*) (List_1_t2522024052 *, const MethodInfo*))List_1_get_Count_m2520315171_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m18886394_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m18886394(__this, ___index0, method) ((  int32_t (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))List_1_get_Item_m18886394_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3230217754_gshared (List_1_t2522024052 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3230217754(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3230217754_gshared)(__this, ___index0, ___value1, method)
